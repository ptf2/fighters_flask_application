from flask import Blueprint
from my_flask_application.models.fighter import *

fighters_app = Blueprint('fighters_app', __name__,
                         static_folder=f'{db_editor.directory}/static',
                         static_url_path=f'{db_editor.directory}/static/')


@fighters_app.route('<int:fighter_id>/parameters/')  # например /fighters/18/parameters/
def fighter_params(fighter_id):

    fighter = Fighter(fighter_id)

    return fighter.most_common_parameters_to_json()


# @fighters_app.route('/<path:path>')
# def static_file(path):
#     return fighters_app.send_static_file(path)

@fighters_app.route('/load_fighters')  # /fighters/load_fighters/
def static_file():

    return fighters_dynamic_panel()
