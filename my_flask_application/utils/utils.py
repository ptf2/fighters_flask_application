import base64


def list_upper(string_list):

    new_list = []
    for item in string_list:
        new_list.append(item.upper())

    return new_list


def read_file_to_base64(file_path):
    if file_path is None:
        return None
    with open(f"{file_path}", "rb") as image:
        file_bin = base64.b64encode(image.read())
        file_utf8 = file_bin.decode('utf-8')
    return file_utf8


def if_not_in_keys(dict_1, dict_2):

    for key in dict_1:

        if key not in dict_2.keys():
            dict_2[key] = None

    return dict_2


