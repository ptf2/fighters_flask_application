from json import JSONEncoder
from my_flask_application.utils.postgres_editor import *
from my_flask_application.utils.utils import *


class Fighter:
    def __init__(self, fighter_number):
        self.fighter_number = fighter_number
        self.id = None
        self.name = None
        self.last_name = None
        self.age = None
        self.height = None
        self.weight = None
        self.country = None
        self.most_common_parameters_from_database()

    def most_common_parameters_from_database(self):
        if self.fighter_number in ids:
            number_in_list = self.fighter_number - 1
            self.id = ids[number_in_list]
            self.name = names[number_in_list]
            self.last_name = last_names[number_in_list]
            self.age = ages[number_in_list]
            self.height = float(heights[number_in_list])
            self.weight = float(weights[number_in_list])
            self.country = countries[number_in_list]

    def most_common_parameters_to_json(self):
        json_enc = JSONEncoder()
        return json_enc.encode({"id": self.id,
                                "name": self.name,
                                "last_name": self.last_name,
                                "age": self.age,
                                "height": self.height,
                                "weight": self.weight})


def put_fighters_full_names_in_dict():
    full_names = {}
    for one in range(len(ids)):
        if names[one] is not None or last_names[one] is not None:
            full_names[ids[one]] = f'{names[one]} {last_names[one]}'
    return full_names


def file_looks_like(image, one):
    if image == 'fighter':
        template = f'{ids[one]}_{names[one]}_{last_names[one]}.png'
        return template

    if image == 'flag':
        template = f'{countries[one]}.png'
        return template

    else:
        return None


def path_to_file_maker(file_path, template, add_host=True, host='127.0.0.1:5000'):
    images = {}

    if add_host:
        host = f'http://{host}'
    else:
        host = ''

    for one in range(len(ids)):

        if os.path.exists(f'{file_path}/{file_looks_like(template, one)}'):
            images[ids[one]] = f'{host}{file_path}/{file_looks_like(template, one)}'

    return images


def fighters_dynamic_panel():
    """формирует JSON с бойцами UFC для fighters_dynamic_panel
    берёт из postgres-базы ufc_fighters_schema поля "id", "name", "lastName", "country"
    парсит путь к "imageFighter", "fighterFlag" при наличии изображений в папке /static
    """

    where_fighters_images = path_to_file_maker(
        #file_path=f"{db_editor.directory}/static/UFC_Fighters/fighters",
        file_path="/home/anton/PycharmProjects/my_flask_application/my_flask_application/static/UFC_Fighters/fighters",
        template='fighter',
        add_host=True)

    where_flags_images = path_to_file_maker(
        #file_path=f"{db_editor.directory}/static/UFC_Fighters/flags",
        file_path="/home/anton/PycharmProjects/my_flask_application/my_flask_application/static/UFC_Fighters/flags",
        template='flag',
        add_host=True)

    full_names_dict = put_fighters_full_names_in_dict()

    big_list = []

    where_fighters_images = if_not_in_keys(ids, where_fighters_images)

    where_flags_images = if_not_in_keys(ids, where_flags_images)

    for number in ids:

        big_list.append(
                {
                        "fighterNumber": number,
                        "name": full_names_dict[number],
                        "imageFighter": where_fighters_images[number],
                        "fighterFlag": where_flags_images[number],
                }
                        )

    json_enc = JSONEncoder()

    return json_enc.encode(big_list)


try:
    db_editor = DatabaseEditor(database="ptf_db",
                               user='postgres',
                               password='123',
                               host="127.0.0.1",
                               port='5432')

    ids = db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='id')
    names = list_upper(db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='name'))
    last_names = list_upper(db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='lastName'))
    ages = db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='age')
    heights = db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='height')
    weights = db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='weight')
    countries = db_editor.read_chosen_column(table_name='ufc_fighters_schema', column_name='country')

except Exception as e:
    logging.error(e)

else:
    db_editor.close_connection()

# print(fighters_dynamic_panel())






