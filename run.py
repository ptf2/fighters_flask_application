from flask import Flask
from my_flask_application.fighters.views import fighters_app

# path.append()

app = Flask(__name__)
app.register_blueprint(fighters_app, url_prefix='/fighters')


if __name__ == '__main__':
    app.run(debug=True)
